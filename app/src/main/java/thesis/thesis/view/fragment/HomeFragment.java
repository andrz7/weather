package thesis.thesis.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.parceler.Parcels;

import java.util.ArrayList;

import thesis.thesis.Constant;
import thesis.thesis.R;
import thesis.thesis.adapter.WeatherListAdapter;
import thesis.thesis.controller.HomeFragmentController;
import thesis.thesis.model.Base;
import thesis.thesis.model.ListContain;

/**
 * Created by Andrea's on 9/21/2016.
 */

public class HomeFragment extends Fragment {

    private RecyclerView weatherListRv;

    private HomeFragmentController controller;
    private LinearLayoutManager layoutManager;
    private WeatherListAdapter adapter;
    private Activity mActivity;

    private ArrayList<ListContain> data;
    private boolean isDataExist = false;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivity = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constant.KEY_PARCEL, Parcels.wrap(data));
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = new ArrayList<>();
        if(savedInstanceState != null){
            data = Parcels.unwrap(savedInstanceState.getParcelable(Constant.KEY_PARCEL));
            isDataExist = true;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        weatherListRv = (RecyclerView) view.findViewById(R.id.weather_rv);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutManager = new LinearLayoutManager(mActivity);
        weatherListRv.setLayoutManager(layoutManager);

        adapter = new WeatherListAdapter(mActivity, data);
        weatherListRv.setAdapter(adapter);

        if(!isDataExist) {
            controller = new HomeFragmentController(this);
            controller.getWeatherList();
        }
    }

    public HomeFragment getsInstance(){
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    public void showWeatherList(Base base){
        if (base != null){
            data = base.getData();
            adapter.setCityData(base.getCity());
            adapter.setData(data);
        }
    }

    public Activity getAttachedActivity(){
        return mActivity;
    }
}
