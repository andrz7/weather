package thesis.thesis.view.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import thesis.thesis.R;
import thesis.thesis.view.fragment.HomeFragment;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_layout, new HomeFragment()).commit();
    }
}
