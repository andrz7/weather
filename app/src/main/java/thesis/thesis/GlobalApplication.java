package thesis.thesis;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Handler;

import thesis.thesis.services.ClientServices;

/**
 * Created by Andrea's on 9/7/2016.
 */
public class GlobalApplication extends Application {
    private Class<? extends Activity> mTopActivityClass;
    private ClientServices client;
    private Context context;
    private Handler mHandler;

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
        context = getApplicationContext();
    }
}
