package thesis.thesis.services;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.QueryMap;
import thesis.thesis.model.Base;

/**
 * Created by Andrea's on 9/6/2016.
 */
public interface ClientServices {

    @GET("/forecast/daily")
    void getWeatherList(@QueryMap Map<String, String> options, Callback<Base> callback);
}
