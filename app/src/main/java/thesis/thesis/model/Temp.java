package thesis.thesis.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Andrea's on 9/21/2016.
 */
@Parcel
public class Temp {

    @SerializedName("day")
    double date;

    @SerializedName("min")
    double min;

    @SerializedName("max")
    double max;

    @SerializedName("night")
    double night;

    @SerializedName("eve")
    double eve;

    @SerializedName("morn")
    double morn;

    public Temp() {
    }

    public Temp(double date, double min, double max, double night, double eve, double morn) {
        this.date = date;
        this.min = min;
        this.max = max;
        this.night = night;
        this.eve = eve;
        this.morn = morn;
    }

    public double getDate() {
        return date;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public double getNight() {
        return night;
    }

    public double getEve() {
        return eve;
    }

    public double getMorn() {
        return morn;
    }
}
