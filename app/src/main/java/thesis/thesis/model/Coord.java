package thesis.thesis.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Andrea's on 9/21/2016.
 */
@Parcel
public class Coord {

    @SerializedName("lat")
    double lat;

    @SerializedName("lon")
    double lon;

    public Coord() {
    }

    public Coord(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
