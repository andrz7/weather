package thesis.thesis.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Andrea's on 9/21/2016.
 */
@Parcel
public class Base {
    @SerializedName("city")
    City city;

    @SerializedName("cod")
    String cod;

    @SerializedName("message")
    String message;

    @SerializedName("cnt")
    int cnt;

    @SerializedName("list")
    ArrayList<ListContain> data;

    public City getCity() {
        return city;
    }

    public ArrayList<ListContain> getData() {
        return data;
    }
}
