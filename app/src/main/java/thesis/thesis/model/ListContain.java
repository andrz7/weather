package thesis.thesis.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Andrea's on 9/21/2016.
 */
@Parcel
public class ListContain {
    @SerializedName("dt")
    long date;

    @SerializedName("temp")
    Temp temp;

    @SerializedName("pressure")
    double pressure;

    @SerializedName("humidity")
    double humidity;

    @SerializedName("weather")
    ArrayList<Weather> weather;

    @SerializedName("speed")
    double speed;

    @SerializedName("deg")
    int deg;

    @SerializedName("clouds")
    int clouds;

    @SerializedName("rain")
    double rain;

    public ListContain() {
    }

    public ListContain(long date, Temp temp, double pressure, double humidity, ArrayList<Weather> weather, double speed, int deg, int clouds, double rain) {
        this.date = date;
        this.temp = temp;
        this.pressure = pressure;
        this.humidity = humidity;
        this.weather = weather;
        this.speed = speed;
        this.deg = deg;
        this.clouds = clouds;
        this.rain = rain;
    }

    public long getDate() {
        return date;
    }

    public Temp getTemp() {
        return temp;
    }

    public double getPressure() {
        return pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public double getSpeed() {
        return speed;
    }

    public int getDeg() {
        return deg;
    }

    public int getClouds() {
        return clouds;
    }

    public double getRain() {
        return rain;
    }
}
