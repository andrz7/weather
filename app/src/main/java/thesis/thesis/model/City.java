package thesis.thesis.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by Andrea's on 9/21/2016.
 */
@Parcel
public class City {

    @SerializedName("id")
    long id;

    @SerializedName("name")
    String name;

    @SerializedName("coord")
    Coord coord;

    @SerializedName("country")
    String country;

    @SerializedName("population")
    int population;

    public City() {
    }

    public City(long id, String name, Coord coord, String country, int population) {
        this.id = id;
        this.name = name;
        this.coord = coord;
        this.country = country;
        this.population = population;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Coord getCoord() {
        return coord;
    }

    public String getCountry() {
        return country;
    }

    public int getPopulation() {
        return population;
    }
}
