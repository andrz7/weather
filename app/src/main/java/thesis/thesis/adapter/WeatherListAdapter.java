package thesis.thesis.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import thesis.thesis.R;
import thesis.thesis.Util;
import thesis.thesis.model.City;
import thesis.thesis.model.ListContain;
import thesis.thesis.model.Temp;
import thesis.thesis.model.Weather;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

/**
 * Created by Andrea's on 9/21/2016.
 */

public class WeatherListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ListContain> data;
    private Context mContext;
    private City city;

    public WeatherListAdapter(Context context, ArrayList<ListContain> data) {
        mContext = context;
        this.data = data;
    }

    public void setData(ArrayList<ListContain> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void setCityData(City city){
        this.city = city;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.weather_list_card_item, parent, false);
        NormalHolder normalHolder = new NormalHolder(view);
        return normalHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
       NormalHolder normalHolder = (NormalHolder) holder;
        normalHolder.cityName.setText(city.getName());

        Temp temp = data.get(position).getTemp();
        normalHolder.temperatureTv.setText(String.format("%d", Util.convertToCelcius(temp.getMax())) + (char) 0x00B0);

        normalHolder.rainPredictionTv.setText(String.valueOf(data.get(position).getRain()) + "%");

        normalHolder.dateTextView.setText(Util.formatDate(data.get(position).getDate()));

        ArrayList<Weather> weathers = data.get(position).getWeather();

        normalHolder.weatherIcon.setImageResource(Util.getIconResourceForWeatherCondition(weathers.get(0).getId()));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class NormalHolder extends RecyclerView.ViewHolder{

        private TextView temperatureTv;
        private TextView cityName;
        private TextView rainPredictionTv;
        private TextView dateTextView;
        private ImageView weatherIcon;

        public NormalHolder(View itemView) {
            super(itemView);
            cityName = (TextView) itemView.findViewById(R.id.city_name_tv);
            temperatureTv = (TextView) itemView.findViewById(R.id.temperature_tv);
            rainPredictionTv = (TextView) itemView.findViewById(R.id.rain_prediction_tv);
            dateTextView = (TextView) itemView.findViewById(R.id.date);
            weatherIcon = (ImageView) itemView.findViewById(R.id.weather_icon);
        }
    }
}
