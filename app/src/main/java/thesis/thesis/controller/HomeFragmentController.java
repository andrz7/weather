package thesis.thesis.controller;

import android.content.Context;
import android.os.Message;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import thesis.thesis.Constant;
import thesis.thesis.GlobalApplication;
import thesis.thesis.handler.HomeHandler;
import thesis.thesis.model.Base;
import thesis.thesis.services.Client;
import thesis.thesis.services.ClientServices;
import thesis.thesis.view.fragment.HomeFragment;

/**
 * Created by Andrea's on 9/21/2016.
 */

public class HomeFragmentController {
    private GlobalApplication mApplication;
    private HomeFragment mFragment;
    private HomeHandler mHandler;
    private Context mContext;

    public HomeFragmentController(HomeFragment mFragment) {
        this.mFragment = mFragment;
        this.mContext = mFragment.getAttachedActivity().getApplicationContext();
        this.mHandler = new HomeHandler(mFragment);
        this.mApplication = (GlobalApplication) mFragment.getAttachedActivity().getApplication();
    }

    public void getWeatherList() {
        Map<String, String> data = new HashMap<>();
        data.put(Constant.MAP_KEY_CITY_ID, Constant.CITY_ID);
        data.put(Constant.MAP_KEY_APP_ID, Constant.API_KEY);
        ClientServices services = Client.getServices(mContext);
        services.getWeatherList(data, new Callback<Base>() {
            @Override
            public void success(Base base, Response response) {
                loadWeatherList(base);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("error", error.getMessage());
            }
        });
    }

    private void loadWeatherList(Base base){
        if (mFragment != null){
            Message message = new Message();
            message.what = HomeHandler.WHAT_DISPLAY_LIST;
            message.obj = base;
            mHandler.sendMessage(message);
        }
    }
}
