package thesis.thesis.controller;

import android.content.Context;
import android.os.Handler;

import thesis.thesis.GlobalApplication;
import thesis.thesis.view.activity.SplashScreenActivity;
import thesis.thesis.handler.SplashScreenHandler;

/**
 * Created by Andrea's on 9/7/2016.
 */
public class SplashScreenController {
    private GlobalApplication globalApplication;
    private SplashScreenActivity mActivity;
    private Handler mHandler;
    private Context context;

    public SplashScreenController(SplashScreenActivity mActivity) {
        this.mActivity = mActivity;
        this.context = mActivity.getApplicationContext();
        this.mHandler = new SplashScreenHandler(mActivity);
        this.globalApplication = (GlobalApplication) mActivity.getApplication();
    }

}
