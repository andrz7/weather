package thesis.thesis.handler;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.v4.app.Fragment;

import thesis.thesis.GlobalApplication;

/**
 * Created by Andrea's on 9/7/2016.
 */
public class BaseHandler<T> extends Handler {
    private Context context;
    private T mParentComponent;
    private GlobalApplication globalApplication;

    @SuppressLint("NewApi")
    public BaseHandler(T mParentComponent) {
        this.mParentComponent = mParentComponent;
        if (mParentComponent instanceof Activity){
            globalApplication = (GlobalApplication) ((Activity) mParentComponent).getApplication();
        }else if (mParentComponent instanceof Fragment){
            globalApplication = (GlobalApplication) ((Fragment) mParentComponent).getActivity().getApplication();
        }else if (mParentComponent instanceof android.app.Fragment){
            globalApplication = (GlobalApplication) ((android.app.Fragment) mParentComponent).getActivity().getApplication();
        }else{
            throw new IllegalArgumentException("Parent component must be instance of activity, fragment, or support fragment");
        }
        context = globalApplication.getApplicationContext();
    }

    @SuppressLint("NewApi")
    private Activity getParentActivity() {
        if(mParentComponent instanceof Activity) {
            return (Activity) mParentComponent;
        } else if(mParentComponent instanceof Fragment) {
            return ((Fragment) mParentComponent).getActivity();
        } else if(mParentComponent instanceof android.app.Fragment) {
            return ((android.app.Fragment) mParentComponent).getActivity();
        } else {
            return null;
        }
    }
}
