package thesis.thesis.handler;

import thesis.thesis.view.activity.SplashScreenActivity;

/**
 * Created by Andrea's on 9/7/2016.
 */
public class SplashScreenHandler extends BaseHandler<SplashScreenActivity>{

    public SplashScreenHandler(SplashScreenActivity mParentComponent) {
        super(mParentComponent);
    }
}
