package thesis.thesis.handler;

import android.os.Message;

import thesis.thesis.model.Base;
import thesis.thesis.view.fragment.HomeFragment;

/**
 * Created by Andrea's on 9/21/2016.
 */

public class HomeHandler extends BaseHandler<HomeFragment> {
    public final static int WHAT_DISPLAY_LIST = 1;

    private HomeFragment mFragment;

    public HomeHandler(HomeFragment mParentComponent) {
        super(mParentComponent);
        this.mFragment = mParentComponent;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case WHAT_DISPLAY_LIST:
                showWeatherList((Base) msg.obj);
                break;
        }
    }

    private void showWeatherList(Base base){
        mFragment.showWeatherList(base);
    }
}
